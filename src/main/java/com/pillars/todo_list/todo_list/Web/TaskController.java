package com.pillars.todo_list.todo_list.Web;

import com.pillars.todo_list.todo_list.Domain.Task;
import com.pillars.todo_list.todo_list.Service.TaskService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class TaskController {
    private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping
    public Task insertTask(@RequestBody Task task){

        taskService.saveTask(task);
        return  task;
    }
    @PutMapping
    public Task UpdateTask(@RequestBody Task task){
        taskService.UpdateTask(task);
        return task;
    }

    @GetMapping("/find-all")
    public List<Task> getAll(){
        return taskService.GetAll();
    }
    @GetMapping("/get-by-id/{id}")
    public Optional<Task> getById(@PathVariable Long id){
        return taskService.GetById(id);
    }
    @GetMapping("/find-by-status/{status}")
    public List<Task> getByStatus(@PathVariable String status){
        return taskService.GetByStatus(status);
    }
    @DeleteMapping("/{id}")
    public void DeleteTask(@PathVariable Long id){
        taskService.DeleteTask(id);

    }
}

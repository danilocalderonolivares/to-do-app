package com.pillars.todo_list.todo_list.Repository;

import com.pillars.todo_list.todo_list.Domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * TaskRepository
 */
public interface TaskRepository  extends JpaRepository<Task,Long> {
    List<Task> findByStatus(String status);
    
}
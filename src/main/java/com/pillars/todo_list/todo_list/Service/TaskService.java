package com.pillars.todo_list.todo_list.Service;

import com.pillars.todo_list.todo_list.Domain.Task;
import com.pillars.todo_list.todo_list.Repository.TaskRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {

        this.taskRepository = taskRepository;
    }

    @Transactional()
    public void saveTask (Task task){
        taskRepository.save(task);
    }

    @Transactional()
    public  void UpdateTask( Task task){
        Task taskUpdated = taskRepository.getOne(task.getId());
        taskUpdated.setCategory(task.getCategory());
        taskUpdated.setDescription(task.getDescription());
        taskUpdated.setStatus(task.getStatus());
        taskUpdated.setCreatedDate(task.getCreatedDate());
        taskRepository.save(taskUpdated);
    }

    @Transactional(readOnly = true)
    public List<Task> GetAll(){
        return taskRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Task> GetById(Long id){

        return taskRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public List<Task> GetByStatus(String status){

        return taskRepository.findByStatus(status);
    }

    @Transactional()
    public  void DeleteTask(Long id){
        taskRepository.deleteById(id);
    }

}
